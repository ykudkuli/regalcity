<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Suits
 * @since Suits 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?php echo(get_site_url()); ?>/script.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/fonts/segoeui.ttf">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body 

<?php body_class(); ?>>
<div id="masterloader">
  <div> 
    <center>
      <img src="<?php bloginfo('template_directory'); ?>/images/ldr.gif"/> 
      <p>Please wait while page loading...</p>
    </center>
  </div>
</div>	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-header-child">
				<span class="home-link" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php $header_image = get_header_image();
					if ( ! empty( $header_image ) && ! display_header_text() ) : ?>
						<img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php endif; ?>
<img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
				</span>
				<div class="head-right-big">
						<p><i class='fa fa-phone'></i><u>+</u>919845044805</p>
				</div>
				<div id="navbar" class="smallnavbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( '<label class="sign_0"><img src="http://regalfacility.co.in/rfc/wp-content/themes/suits/images/bars.png" height="25px" width="27px"/></label><label class="sign_1"><img src="http://www.datafidelis.com/dfs/wp-content/themes/suits/images/arrow.png" height="25px" width="40px"/></label>', 'suits' ); ?></h3>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

			</div>
			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( 'Menu', 'suits' ); ?></h3>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php //get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><!-- #masthead -->
		<div id="fix-gap"></div>
		<div id="main" class="site-main">