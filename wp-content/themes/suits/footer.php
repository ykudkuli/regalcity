<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package Suits
 * @since Suits 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php //get_sidebar( 'footer' ); ?>
  <div id="bottom"> 
    <center>
      <div class="holder"> 
        <div class="tail-center"> 
          <p class="pwdby">Powered by <a href="http://www.icantech.in" target="_blank">ICan Technologies, Mysore</a></p>
          <p class="arr">All Rights Reserved</p>
        </div>
      </div>
    </center>
  </div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>