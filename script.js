$(document).ready(function(){
	$("#masterloader").find("p").html("Please wait while page loading..."); 
		$("body").css("overflow","hidden");
		$(window).load(function() {
				$("body").css("overflow","auto");
				$("#masterloader").fadeOut(500);
		});

	$(document).scroll(function(){
		if($(this).scrollTop()>0)
		{
			$("#site-navigation").removeClass("toggled-on");
			$(".menu-toggle").find(".sign_1").fadeOut(500);
			$(".menu-toggle").find(".sign_0").fadeIn(500);		
		}
/*		if($(this).scrollTop()>0)
		{
			$("#site-navigation menu-toggle").css("line-height", "1");
			$(".head-right-big").css("margin-top", "24px");
			$(".home-link img").css("width", "225px");
			$(".home-link img").css("height", "78px");
			$(".site-header").css("box-shadow", "0px -1px 8px #999999");
		} else {
			$(".site-title-scroll").attr("class", "site-title");
			$("#site-navigation menu-toggle").css("line-height", "2");
			$(".home-link img").css("width", "301px");
			$(".home-link img").css("height", "103px");
			$(".head-right-big").css("margin-top", "44px");
			$(".site-header").css("box-shadow", "0px 0px 0px #999999");
		}*/
	});
	
	$(".menu-toggle").click(function(){
		if($(this).find(".sign_1").is(":hidden"))
		{
			$(this).find(".sign_0").fadeOut(500);
			$(this).find(".sign_1").fadeIn(500);
		}
		if($(this).find(".sign_0").is(":hidden"))
		{
			$(this).find(".sign_1").fadeOut(500);
			$(this).find(".sign_0").fadeIn(500);
		}
	});
	
	$(".menu-menu-container").find("li").click(function(){
		$("#site-navigation").removeClass("toggled-on");
		$(".menu-toggle").find(".sign_1").fadeOut(500);
		$(".menu-toggle").find(".sign_0").fadeIn(500);
		$("#masterloader").fadeIn(500);
	});
});
